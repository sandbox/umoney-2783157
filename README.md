INTRODUCTION
------------
 create qrcode online with logo and embeded qrcode to backgroud image.

INSTALLATION
------------
No special install steps are necessary to use this module, see https://www.drupal.org/documentation/install/modules-themes/modules-8 for further information.

NOTES
-----
When installed, user can get qrcode image by browser http://host//umoneyapi/qrcode?d=yourdata
if input mismatch, system will show a help page guide user how to pass parameter
Note there is no configuaration UI,user should copy own logo file and background file to module installed location
when user input
http://host//umoneyapi/qrcode?bg=mybg.jpg&t=j&size=155&bgtop=1450&bgleft...
he will get a picture with a qrcode image place (1450,250),and qrcode also cantain a logo


MAINTAINERS
-----------
Current maintainers:

 * Umoney (https://www.drupal.org/u/umoney)
